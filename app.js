
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , http = require('http')
  , path = require('path')
  , util = require('util')
  , xml2js = require('xml2js')
  , XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest  
  , OAuth= require('oauth').OAuth;

var app = express();

var consumer  = new OAuth(
	"https://api.twitter.com/oauth/request_token",
	"https://api.twitter.com/oauth/access_token",
	"S1kbvVudbdX1JrkrgQnw7A",
	"bkGb5S5sMGlPQWQf9ENVpqe2fTfR0vbQKzwb31e2g",
	"1.0",
	"http://localhost:3000/sessions/callback",
	"HMAC-SHA1"
);

app.configure(function(){
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  app.use(express.static(path.join(__dirname, 'public'))); 
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser('your secret here'));
  app.use(express.session());
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
  app.use(app.router);
  app.use(require('stylus').middleware(__dirname + '/public'));
  
  app.use(function(req, res, next){
    res.locals.user = req.session.user;
    next();
  });
  
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

app.get('/', routes.index);

http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});

app.get('/sessions/connect', function(req, res){
  consumer.getOAuthRequestToken(function(error, oauthToken, oauthTokenSecret, results){
    if (error) {
      res.send("Error getting OAuth request token : " + util.inspect(error), 500);
    } else {  
      req.session.oauthRequestToken = oauthToken;
      req.session.oauthRequestTokenSecret = oauthTokenSecret;
      res.redirect("https://twitter.com/oauth/authorize?oauth_token="+req.session.oauthRequestToken);      
    }
  });
});

app.get('/sessions/callback', function(req, res){
  
  //Checking in console for session values 
  util.puts(">>"+req.session.oauthRequestToken);
  util.puts(">>"+req.session.oauthRequestTokenSecret);
  util.puts(">>"+req.query.oauth_verifier);
  consumer.getOAuthAccessToken(req.session.oauthRequestToken, req.session.oauthRequestTokenSecret, req.query.oauth_verifier, function(error, oauthAccessToken, oauthAccessTokenSecret, results) {
    if (error) {
      res.send("Error getting OAuth access token : " + util.inspect(error) + "["+oauthAccessToken+"]"+ "["+oauthAccessTokenSecret+"]"+ "["+util.inspect(results)+"]", 500);
    } else {
      req.session.oauthAccessToken = oauthAccessToken;
      req.session.oauthAccessTokenSecret = oauthAccessTokenSecret;
      
      res.redirect('/home');
    }
  });
});

app.get('/home', function(req, res){
    consumer.get("http://twitter.com/account/verify_credentials.json", req.session.oauthAccessToken, req.session.oauthAccessTokenSecret, function (error, data, response) {
      if (error) {
          res.redirect('/sessions/connect');
          // res.send("Error getting twitter screen name : " + util.inspect(error), 500);
      } else {
          var parsedData = JSON.parse(data);
          var xmlData = 'NO DATA';
                                                             
          //Rendering index view file
          
          res.render('index', {
            title : 'admios test',
            user : parsedData.screen_name,
            userPic : parsedData.profile_image_url_https, 
            result: xmlData
          })
          
      } 
    });
});

app.get('/search', function(req, res){
  //Retrieve symbol from url  
  if(req.method === "GET") {
    var stockSymbol = req.query["stockSymbol"];       
    var xmlData = "";
    
    // Getting XML          
    xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      util.puts("State: " + this.readyState);            
      if (this.readyState == 4) {
        //log Complete event and check if we are getting the structure with the length property
        util.puts("Complete.\nBody length: " + this.responseText.length);
        xmlData = this.responseText;
                
        res.send(xmlData);          
      }  
    }                                          
    xhr.open("GET", "http://www.google.com/ig/api?stock="+stockSymbol);
    xhr.send();                
  } 
});

app.get('*', function(req, res){
    res.redirect('/home');
});