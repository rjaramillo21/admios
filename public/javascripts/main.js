jQuery(document).ready(function(){  
  jQuery('#searchSymbol').click(function(){
    var stockSymbol = jQuery('#stockSymbol').val();       
    
    jQuery.ajax({
      type: 'GET',
      url: '/search',
      async: false,
      timeout: 30000,
      data: 'stockSymbol='+stockSymbol,
      dataType: 'text',
      success: function(data){        
        jQuery('#wrapper').text(data);
      }      
    });
    
  });
  

  
});